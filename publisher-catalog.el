;;; org-v8
;;; ------
(defvar org-v8-themes
  '((readtheorg
	 .
 	 "https://gitlab.com/vxcg/pub/orgmode/html-themes/readtheorg.git"
;; 	 "https://gitlab.com/vlead-projects/html-themes/readtheorg.git"	 
	 )))

(defvar org-v8-resources
  `((repo . "https://gitlab.com/vxcg/pub/orgmode/html-exports/org-v8.git")
	(org  . "http://orgmode.org/org-8.3.4.tar.gz")
	(themes . ,org-v8-themes)))


;;; org-v9
;;; ------

(defvar org-v9-themes
  '((readtheorg
	 .
	 "https://gitlab.com/vxcg/pub/orgmode/html-themes/readtheorg.git"
	 ;;	 "https://gitlab.com/vlead-projects/html-themes/readtheorg.git"	 
	 )
	(math
	 . "https://gitlab.com/vxcg/pub/orgmode/html-themes/math.git")))


(defvar org-v9-resources
  `((repo . "https://gitlab.com/vxcg/pub/orgmode/html-exports/org-v9.git")
	(org . "http://orgmode.org/org-9.1.9.tar.gz")
	(themes . ,org-v9-themes)))

;;; git directories
;;; ---------------

;; (defvar org-ref
;;   '((name . "org-ref")
;;     (url  . "https://github.com/jkitchin/org-ref.git")))

;; (defvar emacs-htmlize
;;   '((name . "emacs-htmlize")
;;     (url  . "https://github.com/hniksic/emacs-htmlize")))

(defvar publisher-catalog
  `((org-v8 . ,org-v8-resources)
	(org-v9 . ,org-v9-resources)))
	
(provide 'publisher-catalog)







  
