;;; Summary: installs all resources from catalog
;;; --------

;;; Usage:
;;; ------
;;; emacs -q --script install.el path="<emacs-utils-dir>:." infra="<infra-dir>" 

;;; Effect:
;;; -------
;;; Reads ./catalog.el and installs resources in the alist
;;; catalog-structure.

;;; If infra is skipped at command line, the default is ~/tmp/infra
;;; read cli args into an alist
(message "argv=%s" argv)
(require 'subr-x)
(defvar arg-alist
  (mapcar (lambda (p)
			(let ((ls (split-string p "=")))
			  (cons (string-trim (car ls))
					(string-trim (cadr ls)))))
		  argv))

(print arg-alist)

(setq load-path
	  (let* ((p (assoc "path" arg-alist))
			 (path-str (if p (cdr p) ""))
			 (path-list (split-string path-str ":")))
		(progn
		  (message "path-str = %s" path-str)		
		  (message "path-list = %s" path-list)
		  (append path-list load-path))))


(message "load-path=%s" load-path)

;;; once load-path has been extended,
;;; the below packages are available
(require 'emacs-funs)
(require 'catalog)

;;; infra directory
(defvar infra-dir
  (lookup "infra-dir" arg-alist
		  (concat-as-dir (getenv "HOME")
						 "tmp/infra")))

;;; install the repos in the catalog-structure
(generic-install infra-dir catalog-structure)







	
	
   

	
