;;; catalog

(defvar catalog-publishers
  `(
	(el-publisher  .
	  "https://gitlab.com/vxcg/pub/orgmode/el-publisher.git")))


;;; org-distros
;;; ===========
(defvar org-9-distro '(org-9.1.9 . "http://orgmode.org/org-9.1.9.tar.gz"))
(defvar catalog-org-distros  `(,org-9-distro))


;;; exporters
;;; =========
(defvar org-v9-exporter
   `((repo
 	 . "https://gitlab.com/vxcg/pub/orgmode/html-exports/org-v9.git")
 	(org-distro . ,org-9-distro)))

(defvar catalog-exporters
   `((org-v9 . ,org-v9-exporter)))

;; (defvar catalog-exporters
;;   '((org-v9 . "https://gitlab.com/vxcg/pub/orgmode/html-exports/org-v9.git")))

;;; themes
;;; ======
(defvar catalog-themes '())

(setq catalog-themes
	  `(
		(readtheorg
		 . "https://gitlab.com/vxcg/pub/orgmode/html-themes/readtheorg.git")
		(book
		 . "https://gitlab.com/vxcg/pub/orgmode/html-themes/book.git")

		(popl
		 . "https://gitlab.com/vxcg/pub/orgmode/html-themes/popl.git")
		
		(math
		 . "https://gitlab.com/vxcg/pub/orgmode/html-themes/math.git")

		(vlead-slides
		 . "https://gitlab.com/vxcg/pub/orgmode/html-themes/vlead-slides.git")
		
		(iiit-slides
		 . "https://gitlab.com/vxcg/pub/orgmode/html-themes/iiit-slides.git")	
		(experiment
		 . "https://gitlab.com/vxcg/pub/orgmode/html-themes/experiment.git")
		(org-reveal-iiit
		 . "https://gitlab.com/vxcg/pub/orgmode/html-themes/org-reveal-iiit.git")
		(org-reveal
		 . ((reveal.js . "https://github.com/hakimel/reveal.js.git")
			(Reveal.js-TOC-Progress
			 . "https://github.com/e-gor/Reveal.js-TOC-Progress.git")
			))
		))

(defvar catalog-utils
  `((emacs-utils .
	 "https://gitlab.com/vxcg/pub/emacs/emacs-utils.git")))


(defvar catalog-structure '())

(setq catalog-structure
  `(
	(utils       . ,catalog-utils)
	(org-distros . ,catalog-org-distros)
	(exporters   . ,catalog-exporters)
	(publishers  . ,catalog-publishers)
	(themes      . ,catalog-themes)
	))


	
(provide 'catalog)
	
	
