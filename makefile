SHELL:=/bin/bash

# ASSUMPTION: emacs --version >= 25.x
# CONFIGURABLE
emacs=emacs
infra-dir=~/tmp/infra
emacs-utils-repo=https://gitlab.com/vxcg/pub/emacs/emacs-utils.git
utils-dir=${infra-dir}/utils
emacs-utils-dir=${utils-dir}/emacs-utils
# from command line


all: install-catalog-resources

install-emacs-utils:
	(source ./init.sh; git-install ${emacs-utils-dir} ${emacs-utils-repo})


install-catalog-resources: install-emacs-utils
	(${emacs} -q  --script install.el \ "path=${emacs-utils-dir}:." "infra-dir=${infra-dir}")
